ko.applyBindings(function () {

    var self = this;

    self.isExample2 = ko.observable(true);
    self.questionGroups = [
        { // QuestionGroup 1
            weight: ko.observable(50),
            questions: [
                {
                    weight: ko.observable(10),
                    selectedAnswer: ko.observable(),
                    answers: [
                        { name: 'answer1', value: ko.observable(20)},
                        { name: 'answer2', value: ko.observable(30)},
                        { name: 'answer3', value: ko.observable(40)},
                        { name: 'answer4', value: ko.observable(50)},
                        { name: 'answer5', value: ko.observable(60)},
                    ]
                },
                {
                    weight: ko.observable(20),
                    isCritical: true,
                    selectedAnswer: ko.observable(),
                    answers: [
                        { name: 'answer1', value: ko.observable(20)},
                        { name: 'answer2', value: ko.observable(30)},
                        { name: 'answer3', value: ko.observable(40)},
                    ]
                },
                {
                    weight: ko.observable(30),
                    selectedAnswer: ko.observable(),
                    answers: [
                        { name: 'answer1', value: ko.observable(10)},
                        { name: 'answer2', value: ko.observable(20)},
                        { name: 'answer3', value: ko.observable(30)},
                        { name: 'answer4', value: ko.observable(40)}
                    ]
                }
            ]
        },
        { // QuestionGroup 2
            weight: ko.observable(30),
            isNaEnabled: true,
            isNaSelected: ko.observable(false),
            questions: [
                {
                    weight: ko.observable(10),
                    selectedAnswer: ko.observable(),
                    answers: [
                        { name: 'answer1', value: ko.observable(20)},
                        { name: 'answer2', value: ko.observable(30)},
                        { name: 'answer3', value: ko.observable(40)},
                        { name: 'answer4', value: ko.observable(50)}
                    ]
                },
                {
                    weight: ko.observable(20),
                    selectedAnswer: ko.observable(),
                    answers: [
                        { name: 'answer1', value: ko.observable(30)},
                        { name: 'answer2', value: ko.observable(40)},
                        { name: 'answer3', value: ko.observable(50)},
                    ]
                },
                {
                    weight: ko.observable(30),
                    isCritical: true,
                    selectedAnswer: ko.observable(),
                    answers: [
                        { name: 'answer1', value: ko.observable(40)},
                        { name: 'answer2', value: ko.observable(60)},
                        { name: 'answer3', value: ko.observable(80)}
                    ]
                }
            ]
        },
        { // QuestionGroup 3
            weight: ko.observable(10),
            questions: [
                {
                    weight: ko.observable(10),
                    selectedAnswer: ko.observable(),
                    answers: [
                        { name: 'answer1', value: ko.observable(10)},
                        { name: 'answer2', value: ko.observable(20)},
                        { name: 'answer3', value: ko.observable(30)},
                    ]
                },
                {
                    weight: ko.observable(20),
                    selectedAnswer: ko.observable(),
                    answers: [
                        { name: 'answer1', value: ko.observable(0)},
                        { name: 'answer2', value: ko.observable(10)},
                    ]
                },
                {
                    weight: ko.observable(30),
                    selectedAnswer: ko.observable(),
                    isCritical: true,
                    answers: [
                        { name: 'answer1', value: ko.observable(0)},
                        { name: 'answer2', value: ko.observable(20)}
                    ]
                },
                {
                    weight: ko.observable(30),
                    selectedAnswer: ko.observable(),
                    answers: [
                        { name: 'answer1', value: ko.observable(0)},
                        { name: 'answer2', value: ko.observable(30)}
                    ]
                }
            ]
        },
        { // QuestionGroup 4
            weight: ko.observable(10),
            questions: [
                {
                    weight: ko.observable(10),
                    selectedAnswer: ko.observable(),
                    answers: [
                        { name: 'answer1', value: ko.observable(20)},
                        { name: 'answer2', value: ko.observable(0)},
                    ]
                },
                {
                    weight: ko.observable(20),
                    selectedAnswer: ko.observable(),
                    answers: [
                        { name: 'answer1', value: ko.observable(0)},
                        { name: 'answer2', value: ko.observable(20)},
                    ]
                },
                {
                    weight: ko.observable(30),
                    selectedAnswer: ko.observable(),
                    answers: [
                        { name: 'answer1', value: ko.observable(0)},
                        { name: 'answer2', value: ko.observable(30)}
                    ]
                },
                {
                    weight: ko.observable(30),
                    selectedAnswer: ko.observable(),
                    answers: [
                        { name: 'answer1', value: ko.observable(0)},
                        { name: 'answer2', value: ko.observable(40)}
                    ]
                }
            ]
        }
    ];

    self.totalScore2 = ko.observable(0);
    self.totalCriticalScore2 = ko.observable(0);

    var example2Score = ko.computed(function () {
        self.totalScore2(0);
        self.totalCriticalScore2(0);

        // Calculate the total weight of the enabled groups.
        var totalGroupWeight = 0;
        for (var i = 0; i < self.questionGroups.length; i++) {
            var questionGroup = self.questionGroups[i];
            if (!questionGroup.isNaEnabled || !questionGroup.isNaSelected()) {
                totalGroupWeight += questionGroup.weight();
            }
        }

        // Calculate score for each enabled question group
        for (var i = 0; i < self.questionGroups.length; i++) {
            var questionGroup = self.questionGroups[i];

            var questionGroupScore = 0;
            var questionGroupCriticalScore = 0;

            if (!questionGroup.isNaEnabled || !questionGroup.isNaSelected()) {
                // Calculate score for each question
                for (var j = 0; j < questionGroup.questions.length; j++) {
                    var question = questionGroup.questions[j];
                    if (question.selectedAnswer()) {
                        var answerValue = parseInt(question.selectedAnswer());
                        questionGroupScore += answerValue;

                        if (question.isCritical) {
                            questionGroupCriticalScore += parseInt(question.selectedAnswer());
                        }
                    }
                }
            }

            var groupWeightMultiplier = questionGroup.weight() / totalGroupWeight;
            var weightedGroupScore = questionGroupScore * groupWeightMultiplier;
            var weightedCriticalScore = questionGroupCriticalScore * groupWeightMultiplier;
            self.totalScore2(weightedGroupScore + self.totalScore2());
            self.totalCriticalScore2(weightedCriticalScore + self.totalCriticalScore2());
        }
    });

    self.totalScore3 = ko.observable(0);
    self.totalCriticalScore3 = ko.observable(0);
    self.totalMaxScore3 = ko.observable(0);
    self.totalMaxCriticalScore3 = ko.observable(0);
    self.normalizedScore3 = ko.observable(0);
    self.normalizedCriticalScore3 = ko.observable(0);

    var example3Score = ko.computed(function () {
        self.totalScore3(0);
        self.totalCriticalScore3(0);
        self.totalMaxScore3(0);
        self.totalMaxCriticalScore3(0);

        // Calculate the total weight of the enabled groups.
        var totalGroupWeight = 0;
        for (var i = 0; i < self.questionGroups.length; i++) {
            var questionGroup = self.questionGroups[i];
            if (!questionGroup.isNaEnabled || !questionGroup.isNaSelected()) {
                totalGroupWeight += questionGroup.weight();
            }
        }

        // Calculate score for each enabled question group
        for (var i = 0; i < self.questionGroups.length; i++) {
            var questionGroup = self.questionGroups[i];

            var questionGroupScore = 0;
            var questionGroupCriticalScore = 0;
            var questionGroupMaxScore = 0;
            var questionGroupMaxCriticalScore = 0;

            if (!questionGroup.isNaEnabled || !questionGroup.isNaSelected()) {
                // Calculate score for each question
                for (var j = 0; j < questionGroup.questions.length; j++) {
                    var question = questionGroup.questions[j];

                    var highestValue = 0;
                    for (var k = 0; k < question.answers.length; k++) {
                        var answerValue = parseInt(question.answers[k].value());
                        if (answerValue > highestValue) {
                            highestValue = answerValue;
                        }
                    }

                    if (question.selectedAnswer()) {
                        var answerValue = parseInt(question.selectedAnswer());
                        questionGroupScore += answerValue;
                        questionGroupMaxScore += highestValue;

                        if (question.isCritical) {
                            var selectedAnswer = parseInt(question.selectedAnswer());
                            questionGroupCriticalScore += selectedAnswer;
                            questionGroupMaxCriticalScore += highestValue;
                        }
                    }
                }
            }

            var groupWeightMultiplier = questionGroup.weight() / totalGroupWeight;
            var weightedGroupScore = questionGroupScore * groupWeightMultiplier;
            var weightedGroupMaxScore = questionGroupMaxScore * groupWeightMultiplier;
            var weightedCriticalScore = questionGroupCriticalScore * groupWeightMultiplier;
            var weightedMaxCriticalScore = questionGroupMaxCriticalScore * groupWeightMultiplier;

            self.totalScore3(weightedGroupScore + self.totalScore3());
            self.totalMaxScore3(weightedGroupMaxScore + self.totalMaxScore3());
            self.totalCriticalScore3(weightedCriticalScore + self.totalCriticalScore3());
            self.totalMaxCriticalScore3(weightedMaxCriticalScore + self.totalMaxCriticalScore3());
        }

        self.normalizedScore3(0);
        if (self.totalMaxScore3() > 0) {
            self.normalizedScore3((self.totalScore3() / self.totalMaxScore3() * 100));
        }
        self.normalizedCriticalScore3(0);
        if (self.totalMaxCriticalScore3() > 0) {
            self.normalizedCriticalScore3((self.totalCriticalScore3() / self.totalMaxCriticalScore3() * 100));
        }
    });
}());